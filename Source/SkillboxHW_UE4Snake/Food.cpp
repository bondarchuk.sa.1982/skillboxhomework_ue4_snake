// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

#include "Math/RandomStream.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFood::SetFoodTypeNormal_Implementation()
{
}

void AFood::SetFoodTypeMoveUp_Implementation()
{
}

void AFood::SetFoodTypeMoveDown_Implementation()
{
}

void AFood::MoveFood()
{
	FVector NewPoint(0.f, 0.f, 20.f);

	NewPoint.X = FMath::FRandRange(-450.f, 450.f);
	NewPoint.Y = FMath::FRandRange(-450.f, 450.f);

	this->SetActorLocation(NewPoint);

	SetFoodType();
}

void AFood::SetFoodType()
{
	int NewFoodType = FMath::FRandRange(-10000, 10000);

	if (NewFoodType < -5000) {
		LastFoodType = EFoodType::MOVE_DOWN;
		SetFoodTypeMoveDown();
	}
	else if (NewFoodType > 5000) {
		LastFoodType = EFoodType::MOVE_UP;
		SetFoodTypeMoveUp();
	}
	else {
		LastFoodType = EFoodType::NORMAL;
		SetFoodTypeNormal();
	}
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();

			switch (LastFoodType)
			{
			case EFoodType::MOVE_DOWN: Snake->SpeedDown(); break;
			case EFoodType::MOVE_UP: Snake->SpeedUp();
			case EFoodType::NORMAL:
			default:
				Snake->SpeedUp();   break;
			}

			++totalScore;
			MoveFood();
		}

		if (totalScore == 25) {
			++totalLevel;
			totalScore = 0;

			Snake->ReStartGame(totalLevel);
		}
	}
}

