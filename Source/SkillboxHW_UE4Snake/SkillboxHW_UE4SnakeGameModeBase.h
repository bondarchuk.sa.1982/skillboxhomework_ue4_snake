// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillboxHW_UE4SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOXHW_UE4SNAKE_API ASkillboxHW_UE4SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
