// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillboxHW_UE4Snake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkillboxHW_UE4Snake, "SkillboxHW_UE4Snake" );
