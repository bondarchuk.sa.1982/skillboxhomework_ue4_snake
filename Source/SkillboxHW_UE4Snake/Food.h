// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"

class UStaticMeshComponent;

UENUM()
enum class EFoodType
{
	NORMAL,
	MOVE_UP,
	MOVE_DOWN
};

UCLASS()
class SKILLBOXHW_UE4SNAKE_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		EFoodType LastFoodType = EFoodType::NORMAL;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Food spawn")
		int32 totalScore = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Food spawn")
		int32 totalLevel = 1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetFoodTypeNormal();
		void SetFoodTypeNormal_Implementation();

	UFUNCTION(BlueprintNativeEvent)
		void SetFoodTypeMoveUp();
		void SetFoodTypeMoveUp_Implementation();
		
	UFUNCTION(BlueprintNativeEvent)
		void SetFoodTypeMoveDown();
		void SetFoodTypeMoveDown_Implementation();
		
	UFUNCTION()
		void MoveFood();

	UFUNCTION()
		void SetFoodType();

};
